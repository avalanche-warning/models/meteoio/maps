/******************************************************************************/
/* Copyright 2022 alpine-software.at                                          */
/******************************************************************************/
/* This is free software you can redistribute/modify under the terms of the   */
/* GNU Affero General Public License 3 or later: http://www.gnu.org/licenses  */
/******************************************************************************/

#include <iostream>
#include <stdexcept>
#include <string>

#include <meteoio/MeteoIO.h>

using namespace mio;

int main(int argc, char** argv)
{
	std::cout << "[i] This is " << __FILE__ << "." << std::endl;

	if (argc != 3)
		throw std::invalid_argument("[E] Synopsis: ./interpolation <start_date> <domain>");

	const std::string ini_path(std::string("input/") + argv[2] + std::string("/maps.ini"));
	mio::Config cfg(ini_path);
	mio::IOManager io(cfg);

	std::cout << "[i] Reading DEM..." << std::endl;
	mio::DEMObject dem;
	io.readDEM(dem);

	const double TZ = cfg.get("TIME_ZONE", "Input");
	mio::Date dt;
	mio::IOUtils::convertString(dt, argv[1], TZ);

	std::cout << "[i] Interpolating..." << std::endl;
	std::cout << "    Interpolating on date " << dt.toString(mio::Date::ISO) << std::endl;

	mio::Grid2DObject met_grid;
	/* snow height */
	io.getMeteoData(dt, dem, mio::MeteoData::HS, met_grid);
	io.write2DGrid(met_grid, mio::MeteoGrids::HS, dt);
	/* temperature */
	io.getMeteoData(dt, dem, mio::MeteoData::TA, met_grid);
	io.write2DGrid(met_grid, mio::MeteoGrids::TA, dt);
	/* relative humidity */
	io.getMeteoData(dt, dem, mio::MeteoData::RH, met_grid);
	io.write2DGrid(met_grid, mio::MeteoGrids::RH, dt);
	/* snow surface temperature */
	io.getMeteoData(dt, dem, mio::MeteoData::TSS, met_grid);
	io.write2DGrid(met_grid, mio::MeteoGrids::TSS, dt);
	/* incoming short wave radiation */
	io.getMeteoData(dt, dem, mio::MeteoData::ISWR, met_grid);
	io.write2DGrid(met_grid, mio::MeteoGrids::ISWR, dt);
	/* wind speed */
	io.getMeteoData(dt, dem, mio::MeteoData::VW, met_grid);
	io.write2DGrid(met_grid, mio::MeteoGrids::VW, dt);

	std::cout << "[i] " << __FILE__ << " done" << std::endl;
	return 0;
}
