#!/usr/bin/env python3

################################################################################
# Copyright 2022 alpine-software.at                                            #
################################################################################
# This is free software you can redistribute/modify under the terms of the     #
# GNU Affero General Public License 3 or later: http://www.gnu.org/licenses    #
################################################################################
#This script starts MeteoIO to perform spatial interpolations on station data.

import awio
import awset
from datetime import datetime, timedelta
import glob
import os
import re
import shutil
import sys
import subprocess

_wms_path = "/opt/mapserver/share"

def _add_projections(domain: str):
    grids = glob.glob(f"./grids/{domain}/*.asc")
    dem_prj = os.path.splitext(awset.get(["domain", "dem", "file"], domain))[0] + ".prj"
    if not os.path.exists(dem_prj):
        return
    for grid in grids:
        target = os.path.splitext(grid)[0] + ".prj"
        if not os.path.exists(target):
            shutil.copy2(dem_prj, target)

def _copy_grids(domain: str):
    grids = glob.glob(f"./grids/{domain}/*.asc")
    for grid in grids:
        base = re.search(r"(.*)_(.{1,5}).asc", grid)
        base = base.groups()
        target = f"{_wms_path}/data/{domain}/latest_{base[1]}"
        try:
            os.remove(f"{target}.asc")
            os.remove(f"{target}.prj")
        except:
            pass
        grid_proj = os.path.splitext(grid)[0] + ".prj"
        os.symlink(os.path.abspath(grid), f"{target}.asc")
        os.symlink(os.path.abspath(grid_proj), f"{target}.prj")

def _update_ini(domain: str):
    """Adapt settings of SP nowcasts for maps generation."""
    # remove station listing of operational ini (we use all of them):
    ini_opera = os.path.expanduser(f"~snowpack/opera/input/{domain}/io.ini")
    with open(ini_opera, "r") as file:
        data = file.read()
        data = data.replace("STATION1", "#STATION1")
    with open(ini_opera, "w") as outfile:
        outfile.write(data)
    # update MeteoIO ini file with info for our domain:
    ini_template = f"./input/maps.ini.template"
    with open(ini_template, "r") as file:
        data = file.read()
        data = data.replace("$DOMAIN", domain)
        os.makedirs(f"./input/{domain}", exist_ok=True)
    with open(f"./input/{domain}/maps.ini", "w") as outfile:
        outfile.write(f"# This config file was auto-created by '{__file__}' and can be deleted, but will be re-created.\n")
        outfile.write(data)

def run_domain(interpol_date, domain="default"):
    # NOTE: It is assumed that this script runs _after_ SNOWPACK calculations.
    # Hence, we assume that the necessary data has been downloaded and a
    # working INI file exists.

    os.makedirs(f"./grids/{domain}", exist_ok=True)
    _update_ini(domain)
    # Clear output:
    filelist = awio.multiglob("./grids/{domain}/*.asc", "./grids/{domain}/*.prj")
    for file in filelist:
        os.remove(file)

    # Run calculation powered by MeteoIO's interpolation routines:
    mio_env = os.environ.copy()
    mio_env["LD_LIBRARY_PATH"] = os.path.expanduser("~snowpack/.local/lib")
    sim_prog_path = "./maps"
    print(f"Calling '{sim_prog_path}' for date {interpol_date}...")
    subprocess.call([sim_prog_path, interpol_date, domain], env=mio_env)

    _add_projections(domain)
    _copy_grids(domain)
    
    print(__file__ + " done")

if __name__ == "__main__":
    # defaults:
    domain = "default"
    run_date = datetime.now() - timedelta(hours=12) # a while before the SNOWPACK runs
    dt_format = "%Y-%m-%dT%H:00:00" # for now hope to find data at hh:00:00
    interpol_date = run_date.strftime(dt_format)

    # user input:
    if len(sys.argv) > 3:
        sys.exit("[E] Synopsis: python3 run_interpolations.py [<domain>] [<date>]")
    if len(sys.argv) > 1:
        domain = sys.argv[1]
    if len(sys.argv) > 2:
        interpol_date = sys.argv[2]
    run_domain(interpol_date, domain)
